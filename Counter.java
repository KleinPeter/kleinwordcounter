import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * The Counting functionality program. This class works with the GUI class "wordCounter1".
 *  The class takes a text file as an argument for processing. It parses the file into 
 *  its separate words, counts each of them
 * and their frequency. It can then print the total list of words/frequencies to a 
 * text file in the same directory as this program, as well as sort the top 15 most 
 * frequent words.
 * @author Peter Klein
 * @version 1.0
 */

public class Counter {
	
	//word list contains all the words
	private ArrayList<String> wordList = new ArrayList<String>();
	//word count contains the frequency of each word
	private ArrayList<Integer> wordCount = new ArrayList<Integer>();
	//all words and their counts as strings in a combined list
	private ArrayList<String> combinedList = new ArrayList<String>();
	//hasg map to print all words to a file
	private HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
	
	private int totalWordCount = 0;
	private int totalDifferentWords = 0;
	
	/**
	 * constructor, counts all words and prints it to a file.
	 * It stores all words/frequencies into a hashmap.
	 * It stores all different words into an array list, and their frequencies into
	 * a different array list. the frequency and words match according to index.
	 * 
	 * example: the word "jack" appears 10 times
	 * wordList[1] = "jack"
	 * wordCount[1] = 10
	 * 
	 * @param s
	 *            The file to parse/count.
	 *         
	 */
	
	public Counter(File s){
		try{
			//read the file into the scanner
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(s);
			
			while(scanner.hasNext())
			{
				//grab a word from the scanner, make it lower case
				String word = scanner.next();
				word = word.toLowerCase();
				
				//if the end of the word is punctuation, cut it down.
				while(word.length()>1 && !Character.isLetter(word.charAt(word.length()-1)))
				{
					word = word.substring(0, word.length()-1);
				}
				
				//if the start of the word is punctuation, cut it down.
				while(word.length()>1 && !Character.isLetter(word.charAt(0)))
				{
					word = word.substring(1, word.length());
				}
				
				//add the word to the word list if it is not already in there
				//also add it to the hash map
				//make sure the word is not a single punctuation
				if(Character.isLetter(word.charAt(0)))
				{
					if(!wordList.contains(word))
					{
						wordList.add(word);
						wordCount.add(wordList.indexOf(word), 1);
						hashMap.put(word, 1);
					}
					else
					{
						hashMap.replace(word, hashMap.get(word)+1);
						wordCount.add(wordList.indexOf(word), ((int)wordCount.get(wordList.indexOf(word))+1));
						wordCount.remove(wordList.indexOf(word)+1);		//must remove because add doesnt replace.
					}
					
					//add to the counter of total number of words
					totalWordCount++;
					//get the total number of different words
					totalDifferentWords = wordCount.size();
				}
			}
			
			//print to the file
			printToFile(s);
			
		} catch(FileNotFoundException e){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setContentText("Invalid input");
			alert.showAndWait();
		} catch(IOException e){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setContentText("Invalid input");
			alert.showAndWait();
		}
		
	}
	
	/**
	 * This function takes both the wordList array and the wordCount array, and
	 * sorts/combines these lists in order from largest to smallest,
	 * storing them as strings.
	 * 
	 */
	
	public void sortArrays()
	{
		//sort the word count list of integers, then sort the word
		//list accordingly
		while(0<wordCount.size())
		{
			int largest = 0;
			for(int j = 0; j<wordCount.size(); j++)
			{
				if(wordCount.get(j) > largest)
				{
					largest = wordCount.get(j);
				}
			}
			//add the largest number and its word to the combined list/ hash map
			combinedList.add(largest + "\t\t" +wordList.get(wordCount.indexOf(largest)));
			hashMap.put(wordList.get(wordCount.indexOf(largest)), largest);
			
			//remove the largest word/number
			wordList.remove(wordCount.indexOf(largest));
			wordCount.remove(wordCount.indexOf(largest));
		}
	}
	
	public String getWordArray()
	{
		
		String returnString = "";
		
		for(int i = 0; i<wordList.size(); i++)
		{
			returnString = returnString + wordList.get(i) + "\n";
		}
		return returnString;
		
	}
	
	/**
	 * Takes the hashmap, and prints the words and their frequencies into
	 * a text file, which is saved in the current program directory.
	 * @param name
	 * 				name of the file, used to name the new file
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	
	public void printToFile(File name) throws FileNotFoundException, UnsupportedEncodingException
	{
		//print the hashMap to a file
		sortArrays();
		PrintWriter writer = new PrintWriter(name+"WordCount", "UTF-8");
		
		String printString = hashMap.toString().replaceAll("," , "\r\n");
		writer.println(printString);
		
		writer.close();
	}
	
	/**
	 * getter of total word count
	 * @return
	 * 			the total word count
	 */
	
	public int getTotalNumberofWords()
	{
		return totalWordCount;
	}
	
	/**
	 * getter of total different word count
	 * @return
	 * 			the total diferent word count
	 */
	
	public int getTotalNumberofDWords()
	{
		return totalDifferentWords;
	}
	
	/**
	 * getter of the count array
	 * @return
	 * 			list of total word count in a string
	 */
	
	public String getCountArray()
	{
		String returnString = "";
		
		for(int i = 0; i<wordCount.size(); i++)
		{
			returnString = returnString + wordCount.get(i) + "\n";
		}
		return returnString;
	}
	
	/**
	 * getter of the top 15 most frequent words
	 * @return
	 * 			the top 15 words, in a string
	 */
	
	public String getCombinedArray(){
		
		//first sort array
		sortArrays();
		String returnString = "";
		
		//print out the first 15
		//if there's less than 15, just print out the whole list
		if(combinedList.size() > 15)
		{
			for(int i = 0; i<15; i++)
			{
				returnString = returnString + combinedList.get(i) + "\n";
			}
			returnString = returnString + ". . . \n\n The rest of the words have been printed to a file.";
		} 
		else
		{
			for(int i = 0; i<combinedList.size(); i++)
			{
				returnString = returnString + combinedList.get(i) + "\n";
			}
		}
		
		return returnString;
	}
	
	
}
