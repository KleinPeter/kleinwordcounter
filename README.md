This is my first project for Compilers I. This project will be implemented in Java and have a GUI class and a Counter class. The GUI to prompt the user to select a .txt file. The .txt file will then be sent to the Counter class which will count all of the words in the file. The GUI will then display the top 15 words and their frequencies, as well as the number of total words and number of total different words.

The full list of words and their frequencies will be printed to a word file in the same directory as the program file, under the name �(file name)WordCount�.

to run program:
Compile both "Counter.java" and "wordCounter1.java" and run wordCounter1