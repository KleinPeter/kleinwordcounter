import java.io.File;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * The GUI for a word counting program. This GUI contains a grid with labels and a button.
 * The button allows the user to choose a .txt file. This file is sent to the class,
 * "Counter". Counter counts the total number of words, the total number of different
 * words, and the top 15 most frequent words and their frequencies. All of this information
 * will then be displayed back on this GUI. This class will then print all the words and 
 * their frequencies to a text document which will be named "(original text file)WordCount.txt".
 * 
 * @author Peter Klein
 * @version 1.0
 */


public class wordCounter1 extends Application{
	
	private Button button = new Button();
	private GridPane grid = new GridPane();
	private Label instructions = new Label();
	private Label output = new Label();
	private Label totalWords = new Label();
	private Label totalDifWords = new Label();
	
	/**
	 * starts the program. calls launch.
	 * 
	 * @param args
	 *            command line arguments
	 *         
	 */
	
	public static void main(String[] args){
		launch(args); //calls launch from the class "application"
	}
	
	/**
	 * The driving method.
	 * 
	 * @param primaryStage
	 *            The stage that shows the GUI
	 * @throws Exception
	 * 			  Terminates if exception occurs
	 */
	
	//first thing called from application
	public void start(Stage primaryStage) throws Exception{
		
		//set title
		primaryStage.setTitle("Word Counter");
		
		//set button text
		button.setText("Choose file");
		
		//give instructions label
		instructions.setText("Choose a .txt file!");
		
		//center the grid, give it padding
		grid.setAlignment(Pos.CENTER);
		grid.setPadding(new Insets(10,10,10,10));
		grid.setVgap(10);
		grid.setHgap(10);
		
		//put the input, text field, and labels on the grid
		GridPane.setConstraints(button, 0, 1);
		GridPane.setConstraints(instructions, 0, 0);
		GridPane.setConstraints(totalWords, 0, 2);
		GridPane.setConstraints(totalDifWords, 0, 3);
		GridPane.setConstraints(output, 0, 4);
		
		//put everything on the grid
		grid.getChildren().addAll(button, instructions, output, totalWords, totalDifWords);
		
		//make a scene put in the grid
		Scene scene = new Scene(grid, 500, 500);
		primaryStage.setScene(scene);
		
		//display stage
		primaryStage.show();
		
		//when submit is hit
		button.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent event) {
				
			//get the user to choose a file, only txt
			FileChooser fileChooser = new FileChooser();	
			FileChooser.ExtensionFilter textFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
			fileChooser.getExtensionFilters().add(textFilter);
			File file = fileChooser.showOpenDialog(primaryStage);
			
			
			if(file != null)
			{
				//make a new counter, get an array back
				Counter counter = new Counter(file);
				
				//get the total number of words/total number of different words
				totalWords.setText("Total number of words: "+counter.getTotalNumberofWords());
				totalDifWords.setText("Total number of diferent words: "+counter.getTotalNumberofDWords());
				output.setText(counter.getCombinedArray());
				//set scene
				primaryStage.setScene(scene);
				
				//display stage
				primaryStage.show();
			}
			
			}
		});	
	}
}
